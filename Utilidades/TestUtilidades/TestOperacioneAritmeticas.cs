using Xunit;

namespace TestUtilidades
{
    public class TestOperacioneAritmeticas
    {
        [Fact]
        public void DebeSumarNumerosNegativos()
        {
            Assert.Equal(-8, Utilidades.OperacionesAritmeticas.Sumar(-3, -5));
        }

        [Fact]
        public void DebeSumarNumerosDecimales()
        {
            Assert.Equal(8.1m, Utilidades.OperacionesAritmeticas.Sumar(3.02m, 5.08m));
        }
    }
}